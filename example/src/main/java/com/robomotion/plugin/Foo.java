package com.robomotion.plugin;

import com.robomotion.app.FieldAnnotations;
import com.robomotion.app.Node;
import com.robomotion.app.NodeAnnotations;
import com.robomotion.app.Runtime.Variable;

@NodeAnnotations(id = "Robomotion.Example.Foo", name = "Bar")
public class Foo extends Node 
{
	@FieldAnnotations(title = "Session", type = "integer", scope = "Message", name = "session", messageSCope = true)
	public Variable InSession;
}
